//
//  ViewController.swift
//  Calculator
//
//  Created by Aleksandar Vacić on 27.9.17..
//  Copyright © 2017. Radiant Tap. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	//	UI

	@IBOutlet weak var resultLabel: UILabel!

	@IBOutlet var operatorButtons: [UIButton]!
	@IBOutlet var digitsButtons: [UIButton]!
	@IBOutlet weak var dotButton: UIButton!



	var originalBackgroundColor: UIColor?


}

extension ViewController {
	//	View lifecycle

	override func viewDidLoad() {
		super.viewDidLoad()

		cleanupUI()
		setupTapAction4DigitsButtons()
		setupTapAction4OperatorButtons()
		setupTouchInteraction4Buttons()

		setupDotButton()
	}


	func setupTapAction4DigitsButtons() {
		for btn in digitsButtons {
			btn.addTarget(self,
						  action: #selector(ViewController.digitTapped),
						  for: UIControlEvents.touchUpInside)
		}
	}

	func setupTapAction4OperatorButtons() {
		for btn in operatorButtons {
			btn.addTarget(self,
						  action: #selector(ViewController.operatorTapped),
						  for: UIControlEvents.touchUpInside)
		}
	}

	func cleanupUI() {
		resultLabel.text = nil
	}

	func setupTouchInteraction4Buttons() {
		let allButtons = digitsButtons + operatorButtons + [dotButton]
		for btn in allButtons {
			btn.addTarget(self,
						  action: #selector(ViewController.buttonTouched),
						  for: UIControlEvents.touchDown)
		}
	}
}


extension ViewController {

	func setupDotButton() {
		if let dot = Locale.current.decimalSeparator {
			dotButton.setTitle(dot, for: .normal)
		}
	}

	func untouchButton(_ sender: UIButton) {
		sender.backgroundColor = originalBackgroundColor
	}

	@objc func digitTapped(_ sender: UIButton) {
		defer {
			untouchButton(sender)
		}

		if let s = sender.title(for: .normal) {
			resultLabel.text = s
		}
	}

	@objc func operatorTapped(_ sender: UIButton) {
		defer {
			untouchButton(sender)
		}

	}

	@IBAction func dotTapped(_ sender: UIButton) {
		defer {
			untouchButton(sender)
		}

		if let s = sender.title(for: .normal) {
			if let lbltext = resultLabel.text {
				if lbltext.contains(s) {
					return
				}
				resultLabel.text = lbltext + s
			}
		}
	}

	@objc func buttonTouched(_ sender: UIButton) {
		if let bgcolor = sender.backgroundColor {
			originalBackgroundColor = bgcolor
			let newcolor = bgcolor.withAlphaComponent(0.9)
			sender.backgroundColor = newcolor
		}
	}

}
